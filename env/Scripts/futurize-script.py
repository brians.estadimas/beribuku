#!d:\programming\webdev\project\beribuku_development\beribuku_com-development-v2.0-66fe153891d950ac4a2d0c9bb249148f45741439\env\scripts\python.exe
# EASY-INSTALL-ENTRY-SCRIPT: 'future==0.14.3','console_scripts','futurize'
__requires__ = 'future==0.14.3'
import re
import sys
from pkg_resources import load_entry_point

if __name__ == '__main__':
    sys.argv[0] = re.sub(r'(-script\.pyw?|\.exe)?$', '', sys.argv[0])
    sys.exit(
        load_entry_point('future==0.14.3', 'console_scripts', 'futurize')()
    )
