from django.conf import settings
from django.conf.urls import *
from django.conf.urls.static import static
from django.contrib import admin
from rest_framework import routers

from beribuku_app.views.user.utils import FotoProgramViewSet

router = routers.DefaultRouter()
router.register(r'fotos', FotoProgramViewSet)

handler404  = 'beribuku_app.controller.user.index.handler404'

urlpatterns = [

    url(r'^', include('beribuku_app.urls', namespace='beribuku_app')),
    url(r'^simple-blog/', include('article.urls')),
    url(r'^administration/', include(admin.site.urls)),
        
]
