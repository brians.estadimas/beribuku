from django.conf import settings
from django.conf.urls import *
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth.views import login, logout
from rest_framework import routers
from beribuku_app.views.user.utils import FotoProgramViewSet
from beribuku_app.views.user import index as user_index
from beribuku_app.views.user import program as user_program
from beribuku_app.views.admin import donasi as admin_donasi
from beribuku_app.views.admin import donatur as admin_donatur
from beribuku_app.views.admin import laporan as admin_laporan
from beribuku_app.views.admin import login as admin_login
from beribuku_app.views.admin import main as admin_main
from beribuku_app.views.admin import master as admin_master
from beribuku_app.views.admin import program as admin_program

router = routers.DefaultRouter()
router.register(r'fotos', FotoProgramViewSet)

handler404 = 'beribuku_app.controller.user.index.handler404'

urlpatterns = [
                  url(r'^$', user_index.index, name='user-index'),
                  url(r'^home/', user_index.home, name='user-home'),
                  url(r'^donasi/', user_index.donasi, name='user-donasi'),
                  url(r'^blog/', user_index.blog, name='user-blog'),
                  # url(r'^authenticate/$', beribuku_app.controller.user.index.authenticate, name='authenticate'),
                  # url(r'^proyek/', beribuku_app.controller.user.program.index, name='program'),
                  url(r'^proyek/(?P<id>\d+)/$', user_program.detil, name='program.detil'),
                  url(r'^daftar-proyek/$', user_program.all, name='program-all'),
                  #
                  url(r'^about/', user_index.about, name='user-about'),
                  url(r'^tim/', user_index.tim, name='user-tim'),
                  # url(r'^contact/', beribuku_app.controller.user.index.contact, name='contact'),
                  # url(r'^addDonatur/$', beribuku_app.controller.user.index.addDonatur, name='user.donatur.new'),
                  # url(r'^subscribe/$', beribuku_app.controller.user.index.subscribe, name='user.subscribe'),
                  #
                  url(r'^administration/', include(admin.site.urls)),
                  # url(r'^logout/$', logout, {'next_page': '/admin/login/'}, name='logout'),
                  #
                  # # url admin
                  #
                  url(r'^admin/login/', login, name='admin.login'),
                  #
                  url(r'^admin/logout/', admin_login.logout, name='admin.logout'),

                  url(r'^admin/home/', admin_main.home, name='admin.home'),

                  url(r'^admin/master/', admin_master.index, name='admin.master'),
                  url(r'^admin/newTim/', admin_master.addNewTim, name='admin.master.new'),
                  url(r'^admin/editTim/$', admin_master.editTim, name='admin.master.tim.edit'),
                  url(r'^admin/timDelete/$', admin_master.deleteTim,
                      name='admin.master.deleteTim'),

                  url(r'^admin/newRekening/', admin_master.addNewRekening,
                      name='admin.master.newRekening'),
                  url(r'^admin/editRekening/$', admin_master.editRekening,
                      name='admin.master.rekening.edit'),
                  url(r'^admin/rekDelete/$', admin_master.deleteRekening,
                      name='admin.master.deleteRekening'),

                  url(r'^admin/newAbout/', admin_master.addNewAbout,
                      name='admin.master.newAbout'),
                  url(r'^admin/aboutDelete/$', admin_master.deleteAbout,
                      name='admin.master.deleteAbout'),

                  url(r'^admin/mail/lpj/(?P<programId>\d+)/$', admin_main.mail_lpj,
                      name='admin.mail.lpj'),
                  url(r'^admin/mail/proposal/(?P<programId>\d+)/$', admin_main.mail_proposal,
                      name='admin.mail.proposal'),

                  url(r'^admin/program/$', admin_program.index, name='admin.program'),
                  url(r'^admin/program/(?P<alert>True)/(?P<alert_msg>.*)/$',
                      admin_program.index, name='admin.program.alert'),
                  url(r'^admin/new/', admin_program.addNewProgram, name='admin.program.new'),
                  url(r'^admin/view/$', admin_program.viewProgram, name='admin.program.view'),
                  url(r'^admin/delete/$', admin_program.deleteProgram,
                      name='admin.program.delete'),
                  url(r'^admin/edit/$', admin_program.editProgram, name='admin.program.edit'),
                  url(r'^admin/exceldonatur/(?P<programId>\d+)/$',
                      admin_program.downloadExcelDonatur, name='admin.program.exceldonatur'),
                  url(r'^admin/word/(?P<programId>\d+)/$', admin_program.downloadWord,
                      name='admin.program.word'),

                  url(r'^admin/donatur/$', admin_donatur.index, name='admin.donatur'),
                  url(r'^admin/donatur/(?P<alert>True)/(?P<alert_msg>.*)/$',
                      admin_donatur.index, name='admin.donatur'),
                  url(r'^admin/addDonatur/$', admin_donatur.addDonatur,
                      name='admin.donatur.new'),
                  url(r'^admin/addDonatur/(?P<programId>\d+)/$', admin_donatur.addSpecDonatur,
                      name='admin.donatur.newspec'),
                  url(r'^admin/deleteDonatur/$', admin_donatur.deleteDonatur,
                      name='admin.donatur.delete'),
                  url(r'^admin/editDonatur/$', admin_donatur.editDonatur,
                      name='admin.donatur.edit'),

                  url(r'^admin/donasi/new/(?P<programId>\d+)/$', admin_donasi.addDonasi,
                      name='admin.donasi.new'),

                  url(r'^admin/donasi/uang/view/(?P<programId>\d+)/$',
                      admin_donasi.viewDonasiUang, name='admin.donasi.uang.view'),
                  url(r'^admin/donasi/uang/view/(?P<programId>\d+)/(?P<alert>True)/(?P<alert_msg>.*)/$',
                      admin_donasi.viewDonasiUang, name='admin.donasi.uang.view'),
                  url(r'^admin/donasi/uang/delete/(?P<donasi_id>\d+)/$',
                      admin_donasi.deleteDonasiUang, name='admin.donasi.uang.delete'),
                  url(r'^admin/donasi/uang/edit/(?P<donasi_id>\d+)/$',
                      admin_donasi.editDonasiUang, name='admin.donasi.uang.edit'),

                  url(r'^admin/donasi/buku/view/(?P<programId>\d+)/$',
                      admin_donasi.viewDonasiBuku, name='admin.donasi.buku.view'),
                  url(r'^admin/donasi/buku/view/(?P<programId>\d+)/(?P<alert>True)/(?P<alert_msg>.*)/$',
                      admin_donasi.viewDonasiBuku, name='admin.donasi.buku.view'),
                  url(r'^admin/donasi/buku/delete/(?P<donasi_id>\d+)/$',
                      admin_donasi.deleteDonasiBuku, name='admin.donasi.buku.delete'),
                  url(r'^admin/donasi/buku/edit/(?P<donasi_id>\d+)/$',
                      admin_donasi.editDonasiBuku, name='admin.donasi.buku.edit'),

                  url(r'^admin/donasi/excel/new/(?P<programId>\d+)/$',
                      admin_donasi.addDonasiExcel, name='admin.donasi.new.excel'),

                  url(r'^admin/laporan/$', admin_laporan.index, name='admin.laporan.index'),
                  url(r'^admin/laporan/realisasi/(?P<programId>\d+)/$',
                      admin_laporan.addKeteranganLaporan, name='admin.laporan.add.keterangan'),
                  url(r'^admin/addLaporan/$', admin_laporan.addLaporan,
                      name='admin.laporan.new'),

              ] + static(settings.STATIC_URL, document_root=settings.STATIC_PATH)