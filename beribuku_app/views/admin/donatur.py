from beribuku_app.views.admin.login import *
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required

from beribuku_app.models import *
from beribuku_app.views import validator

@login_required
def index(request, alert=False, alert_msg=""):
    
    data = {
        'donatur': Donatur.objects.all().order_by('-program'), 
        'alert_success': alert,
        'alert_msg' : alert_msg,
        'ABS_URL' : request.get_host(),
        'admin_donatur_active': 'active'
    }

    return render(request, 'admin/donatur/donatur.html', data)


def addDonatur(request):

    if request.method == 'POST':
        programId = request.POST.get('program')
        program = get_object_or_404(Program, pk=programId)
        nama = request.POST.get('nama')
        email = request.POST.get('email')
        nohp = request.POST.get('nohp')
        bukti = None
        if request.FILES:
            bukti = request.FILES['buktitransfer']

        vNama = True
        vEmail = True
        vNoHp = True
        vBukti = True

        nama = validator.escape(nama)
        email = validator.escape(email)
        nohp = validator.escape(nohp)

        if validator.is_empty(nama):
            vNama = False

        if validator.is_empty(email) or not validator.is_email(email):
            vEmail = False

        if validator.is_empty(nohp) or not validator.is_number(nohp):
            vNoHp = False

        if validator.is_empty(bukti) or not validator.is_image(bukti.content_type):
            vBukti = False

        valid = vNama and vBukti and vEmail and vNoHp

        if valid:
            donatur = Donatur(program=program, nama=nama, email=email, bukti=bukti, nohp=nohp)
            donatur.save()

            alert_msg = "Berhasil menambahkan donatur " + nama

            return redirect(reverse('admin.donatur', args=(True, alert_msg,)))
        else:
            err_msg = []

            if not vNama:
                err_msg.append("Bagian 'Nama' tidak boleh kosong")

            if not vEmail:
                err_msg.append("Bagian 'Email' harus diisi dengan email yang valid")

            if not vNoHp:
                err_msg.append("Bagian 'No. Handphone' harus diisi dengan no. HP yang valid")

            if not vBukti:
                err_msg.append("Bagian 'Bukti Transfer' harus diisi dengan file gambar")

            data = {
                'programId' : programId,
                'nama' : nama,
                'email' : email,
                'nohp' : nohp,
                'error' : err_msg,
                'program' : Program.objects.all(),
                'admin_donatur_active': 'active'
            }

            return render(request, 'admin/donatur/newDonatur.html', data)
    else:
        return render(request, 'admin/donatur/newDonatur.html', {'program' : Program.objects.all()})


def addSpecDonatur(request, programId):

    if request.method == 'POST':
        program = get_object_or_404(Program, pk=programId)
        nama = request.POST.get('nama')
        email = request.POST.get('email')
        nohp = request.POST.get('nohp')
        bukti = None
        if request.FILES:
            bukti = request.FILES['buktitransfer']

        vNama = True
        vEmail = True
        vBukti = True
        vNoHp = True

        nama = validator.escape(nama)
        email = validator.escape(email)
        nohp = validator.escape(nohp)

        if validator.is_empty(nama):
            vNama = False

        if validator.is_empty(email) or not validator.is_email(email):
            vEmail = False

        if validator.is_empty(nohp) or not validator.is_number(nohp):
            vNoHp = False

        if validator.is_empty(bukti) or not validator.is_image(bukti.content_type):
            vBukti = False

        valid = vNama and vBukti and vEmail and vNoHp

        if valid:
            donatur = Donatur(program=program, nama=nama, email=email, bukti=bukti, nohp=nohp)
            donatur.save()

            alert_msg = "Berhasil menambahkan donatur " + nama + " pada program " + program.nama

            return redirect(reverse('admin.program', args=(True, alert_msg,)))
        else:
            err_msg = []

            if not vNama:
                err_msg.append("Bagian 'Nama' tidak boleh kosong")

            if not vEmail:
                err_msg.append("Bagian 'Email' harus diisi dengan email yang valid")

            if not vNoHp:
                err_msg.append("Bagian 'No. Handphone' harus diisi dengan no. HP yang valid")

            if not vBukti:
                err_msg.append("Bagian 'Bukti Transfer' harus diisi dengan file gambar")

            data = {
                'programId' : programId,
                'nama' : nama,
                'email' : email,
                'nohp' : nohp,
                'error' : err_msg,
                'program' : Program.objects.all(),
                'admin_donatur_active': 'active'
            }

            return render(request, 'admin/donatur/newDonatur.html', data)
    else:
        program = get_object_or_404(Program, pk=programId)
        return render(request, 'admin/donatur/newSpecDonatur.html', {'program' : Program.objects.all(), 'p' : program})


def editDonatur(request):

    if request.method == 'POST':
        donaturId = request.GET.get('donaturId')
        programId = request.POST.get('program')
        program = get_object_or_404(Program, pk=programId)
        nama = request.POST.get('nama')
        email = request.POST.get('email')
        nohp = request.POST.get('nohp')

        valid = True
        vNama = True
        vEmail = True
        vBukti = True
        vNoHp = True

        nama = validator.escape(nama)
        email = validator.escape(email)
        nohp = validator.escape(nohp)

        if validator.is_empty(nama):
            vNama = False

        if validator.is_empty(email) or not validator.is_email(email):
            vEmail = False

        if validator.is_empty(nohp) or not validator.is_number(nohp):
            vNoHp = False

        if request.FILES:
            if not validator.is_image(request.FILES['buktitransfer']):
                vBukti = False

        valid = vNama and vBukti and vEmail

        if valid:
            donatur = get_object_or_404(Donatur, pk=donaturId)

            donatur.program = program
            donatur.nama = nama
            donatur.email = email
            donatur.nohp = nohp

            if request.FILES:
                donatur.bukti = request.FILES['buktitransfer']

            donatur.save()

            alert_msg = "Berhasil mengubah donatur " + nama

            return redirect(reverse('admin.donatur', args=(True, alert_msg,)))

        else:

            err_msg = []

            if not vNama:
                err_msg.append("Bagian 'Nama' tidak boleh kosong")

            if not vEmail:
                err_msg.append("Bagian 'Email' harus diisi dengan email yang valid")

            if not vNoHp:
                err_msg.append("Bagian 'No. Handphone' harus diisi dengan no. HP yang valid")

            if not vBukti:
                err_msg.append("Bagian 'Bukti Transfer' harus diisi dengan file gambar")

            data = {
                'donatur' : Donatur.objects.get(id=request.GET.get('donaturId')),
                'program' : Program.objects.all(),
                'error' : err_msg,
                'admin_donatur_active': 'active'
            }

            return render(request, 'admin/donatur/editDonatur.html', data)

    elif request.method == 'GET':
        data = {
            'program' : Program.objects.all(),
            'donatur' : Donatur.objects.get(id=request.GET.get('donaturId')),
            'admin_donatur_active': 'active'
        }

        return render(request, 'admin/donatur/editDonatur.html', data)

def deleteDonatur(request):

    data = {}
    if request.method == 'GET':
        donaturId = request.GET.get('id')
        try:
            p = get_object_or_404(Donatur, pk=donaturId)
            alert_msg = 'Berhasil menghapus donatur ' + p.nama
            p.delete()

            return redirect(reverse('admin.donatur', args=(True, alert_msg,)))

        except Exception as e:
            #Program.objects.filter().delete()
            print(e)
        return redirect('/admin/donatur/')