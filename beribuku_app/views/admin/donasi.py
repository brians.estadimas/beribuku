import datetime

import os
import xlrd
from beribuku_app.views.admin.login import *
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404

from beribuku_app.views import validator


def viewDonasiUang(request, programId, alert=True, alert_msg=""):
	if auth(request):

		donasi_list 		= []
		donasi_uang_list 	= []
		donasi_list_piece 	= []
		program = get_object_or_404(Program, id=programId)
		donatur = program.donatur_set.all()

		for d in donatur:
			donasi = Donasi_donatur.objects.filter(donatur=d)
			donasi_list.append(donasi)

		for d in donasi_list:
			donasi_uang = Donasi_uang.objects.filter(donasi_donatur=d)
			donasi_uang_list.append(donasi_uang)
			# donasi_buku = Donasi_buku.objects.filter(donasi_donatur=d)

		for datas in donasi_uang_list:
			for data in datas:
				donasi_list_piece.append(data)
		data={'donasi_uang': donasi_list_piece, 'program': program, 'alert_msg' : alert_msg, 'alert_success' : alert, 'ABS_URL' : request.get_host(),
        'admin_proyek_active' : 'active'}
		return render(request,'admin/donasi/viewDonasiUang.html', data)

def deleteDonasiUang(request, donasi_id):

	data = {'admin_proyek_active' : 'active'}

	if auth(request):
		if request.method == 'GET':

			try:

				donasi_uang 		= get_object_or_404(Donasi_uang, id=donasi_id)
				donasi_donatur 		= donasi_uang.donasi_donatur
				donatur 			= donasi_donatur.donatur
				program 			= donatur.program

				print(str(donasi_uang.id) + " : " + str(donasi_uang.jumlah_uang))
				print(str(donasi_donatur.id) + " : " + donasi_donatur.via)
				print(str(donatur.id) + " : " + donatur.nama)
				print(str(program.id) + " : " + program.nama)

				alert_msg 			= 'Berhasil menghapus donasi ' + donasi_uang.jenis + ' sebesar ' + str(donasi_uang.jumlah_uang) + ' dari donatur ' + donatur.nama
				programId 			= program.id

				donasi_uang.delete()
				donasi_donatur.delete()

				return redirect(reverse('admin.donasi.uang.view', args=(programId, True, alert_msg,)))
			except Exception as e:
				print(e)
				return redirect('/admin/login/')
	else:
 		return redirect('/admin/login/')

def editDonasiUang(request, donasi_id):

	data = {'admin_proyek_active' : 'active'}
	error_msg = None

	if auth(request):
		if request.method == 'GET':
			try:
				donasi_uang 		= get_object_or_404(Donasi_uang, id=donasi_id)
				donasi_donatur 		= donasi_uang.donasi_donatur
				donatur_select 		= donasi_donatur.donatur
				program 			= donatur_select.program
				donatur_all 		= program.donatur_set.all()

				print(str(donasi_uang.id) + " : " + str(donasi_uang.jumlah_uang))
				print(str(donasi_donatur.id) + " : " + donasi_donatur.via)
				print(str(donatur_select.id) + " : " + donatur_select.nama)
				print(str(program.id) + " : " + program.nama)


				alert_msg 			= 'Berhasil mengubah donasi ' + donasi_uang.jenis + ' sebesar ' + str(donasi_uang.jumlah_uang) + ' dari donatur ' + donatur_select.nama
				programId 			= program.id

				data = {'program': program, 'donasi_donatur': donasi_donatur, 'donasi_uang': donasi_uang, 'donatur': donatur_all, 'donatur_select': donatur_select, 'ABS_URL' : request.get_host(),
        'admin_proyek_active' : 'active'}

				return render(request, 'admin/donasi/editDonasiUang.html', data)
			except Exception as e:
				print(e)
				return redirect('/admin/login/')

		if request.method == 'POST':

			try:
				bukti_transfer 		= request.FILES['bukti_transfer']
			except:
				bukti_transfer = None

			try:

				tanggal 			= validator.escape(request.POST.get('tanggal'))
				via 				= validator.escape(request.POST.get('via'))
				konfirmasi			= False
				msg 				= None

				if not request.POST.get('konfirmasi') == None:
					konfirmasi = True

				bank 				= validator.escape(request.POST.get('nama_bank'))
				nominal 			= validator.escape(request.POST.get('nominal'))
				print(bukti_transfer)

				vTanggal			= not validator.is_empty(tanggal)
				vVia				= not validator.is_empty(via)
				vBank				= not validator.is_empty(bank)
				vNominal			= not validator.is_empty(nominal) and validator.is_number(nominal)
				valid 				= vTanggal and vVia and vBank and vNominal

				id_donatur 			= request.POST.get('donatur')

				donatur_select 		= get_object_or_404(Donatur, id=id_donatur)
				donasi_uang		= get_object_or_404(Donasi_uang, id=donasi_id)

				if valid:
					donasi_uang.jumlah_uang 	= nominal

					if not bukti_transfer == None:
						temp 					   = donasi_uang.bukti_transfer
						donasi_uang.bukti_transfer = bukti_transfer
						os.remove(str(temp))

					donasi_uang.bank 			= bank
					donasi_uang.save()

					donasi_donatur 				= donasi_uang.donasi_donatur
					donasi_donatur.konfirmasi 	= konfirmasi
					donasi_donatur.tanggal 		= tanggal
					donasi_donatur.via 			= via
					donasi_donatur.donatur 		= donatur_select
					donasi_donatur.save()

					print('tanggal : ' + str(tanggal) )
					print('via : ' + via)
					print('konfirmasi : ' + str(konfirmasi))
					print('bank : ' + bank)
					print('nominal : ' + str(nominal))
					print('donatur : ' + str(id_donatur))
					print('bukti transfer : ' + str(bukti_transfer))

					alert_msg 			= 'Berhasil mengubah donasi ' + donasi_uang.jenis + ' sebesar ' + str(donasi_uang.jumlah_uang) + ' dari donatur ' + donatur_select.nama
					programId 			= donatur_select.program.id

					return redirect(reverse('admin.donasi.uang.view', args=(programId, True, alert_msg,)))
				else:
					error_msg.append('Data "Donasi Uang" tidak valid')

				# data = {'program': program, 'donasi_donatur': donasi_donatur, 'donasi_uang': donasi_uang, 'donatur': donatur_all, 'donatur_select': donatur_select, 'error_msg': error_msg}
				return render(request, 'admin/donasi/editDonasiUang.html')
			except Exception as e:
				print(e)
				return redirect('/admin/login/')
	else:
 		return redirect('/admin/login/')


def viewDonasiBuku(request, programId, alert=True, alert_msg=""):
	if auth(request):

		donasi_list 		= []
		donasi_buku_list 	= []
		donasi_list_piece 	= []
		program = get_object_or_404(Program, id=programId)
		donatur = program.donatur_set.all()

		for d in donatur:
			donasi = Donasi_donatur.objects.filter(donatur=d)
			donasi_list.append(donasi)

		for d in donasi_list:
			donasi_buku = Donasi_buku.objects.filter(donasi_donatur=d)
			donasi_buku_list.append(donasi_buku)

		for datas in donasi_buku_list:
			for data in datas:
				donasi_list_piece.append(data)
		data={'donasi_buku': donasi_list_piece, 'program': program, 'alert_msg' : alert_msg, 'alert_success' : alert,
        'admin_proyek_active' : 'active'}
		return render(request,'admin/donasi/viewDonasiBuku.html', data)


def editDonasiBuku(request, donasi_id):

	data = {'admin_proyek_active' : 'active'}
	error_msg = None

	if auth(request):
		if request.method == 'GET':
			try:
				donasi_buku 		= get_object_or_404(Donasi_buku, id=donasi_id)
				donasi_donatur 		= donasi_buku.donasi_donatur
				donatur_select 		= donasi_donatur.donatur
				program 			= donatur_select.program
				donatur_all 		= program.donatur_set.all()

				print(str(donasi_buku.id) + " : " + str(donasi_buku.keterangan))
				print(str(donasi_donatur.id) + " : " + donasi_donatur.via)
				print(str(donatur_select.id) + " : " + donatur_select.nama)
				print(str(program.id) + " : " + program.nama)


				alert_msg 			= 'Berhasil mengubah donasi ' + donasi_buku.jenis + ' sebesar ' + str(donasi_buku.keterangan) + ' dari donatur ' + donatur_select.nama
				programId 			= program.id

				data = {'program': program, 'donasi_donatur': donasi_donatur, 'donasi_buku': donasi_buku, 'donatur': donatur_all, 'donatur_select': donatur_select, 'ABS_URL' : request.get_host(),
        'admin_proyek_active' : 'active'}

				return render(request, 'admin/donasi/editDonasiBuku.html', data)

			except Exception as e:

				print(e)
				return redirect('/admin/login/')

		if request.method == 'POST':


			try:

				tanggal 			= validator.escape(request.POST.get('tanggal'))
				via 				= validator.escape(request.POST.get('via'))
				konfirmasi			= False
				msg 				= None

				if not request.POST.get('konfirmasi') == None:
					konfirmasi = True

				keterangan 			= validator.escape(request.POST.get('keterangan'))

				vTanggal			= not validator.is_empty(tanggal)
				vVia				= not validator.is_empty(via)
				vKeterangan			= not validator.is_empty(keterangan)
				valid 				= vTanggal and vVia and vKeterangan

				id_donatur 			= request.POST.get('donatur')

				donatur_select 		= get_object_or_404(Donatur, id=id_donatur)
				donasi_buku			= get_object_or_404(Donasi_buku, id=donasi_id)

				if valid:
					donasi_buku.keterangan 		= keterangan
					donasi_buku.save()

					donasi_donatur 				= donasi_buku.donasi_donatur
					donasi_donatur.konfirmasi 	= konfirmasi
					donasi_donatur.tanggal 		= tanggal
					donasi_donatur.via 			= via
					donasi_donatur.donatur 		= donatur_select
					donasi_donatur.save()

					print('tanggal : ' + str(tanggal) )
					print('via : ' + via)
					print('konfirmasi : ' + str(konfirmasi))
					print('keterangan : ' + str(keterangan))
					print('donatur : ' + str(id_donatur))

					alert_msg 			= 'Berhasil mengubah donasi ' + donasi_buku.jenis + ' sebesar ' + str(donasi_buku.keterangan) + ' dari donatur ' + donatur_select.nama
					programId 			= donatur_select.program.id

					return redirect(reverse('admin.donasi.buku.view', args=(programId, True, alert_msg,)))
				else:
					error_msg.append('Data "Donasi Buku" tidak valid')

				# data = {'program': program, 'donasi_donatur': donasi_donatur, 'donasi_uang': donasi_uang, 'donatur': donatur_all, 'donatur_select': donatur_select, 'error_msg': error_msg}
				return render(request, 'admin/donasi/editDonasiBuku.html')
			except Exception as e:
				print(e)
				return redirect('/admin/login/')
	else:
 		return redirect('/admin/login/')

def deleteDonasiBuku(request, donasi_id):

	data = {'admin_proyek_active' : 'active'}
	if auth(request):
		if request.method == 'GET':

			try:
				donasi_buku 		= get_object_or_404(Donasi_buku, id=donasi_id)
				donasi_donatur 		= donasi_buku.donasi_donatur
				donatur 			= donasi_donatur.donatur
				program 			= donatur.program

				print(str(donasi_buku.id) + " : " + str(donasi_buku.keterangan))
				print(str(donasi_donatur.id) + " : " + donasi_donatur.via)
				print(str(donatur.id) + " : " + donatur.nama)
				print(str(program.id) + " : " + program.nama)

				alert_msg 			= 'Berhasil menghapus donasi ' + donasi_buku.jenis + ' ' + str(donasi_buku.keterangan) + ' dari donatur ' + donatur.nama
				programId 			= program.id

				donasi_buku.delete()
				donasi_donatur.delete()

				return redirect(reverse('admin.donasi.buku.view', args=(programId, True, alert_msg,)))
			except Exception as e:
				print(e)
				return redirect('/admin/login/')
	else:
 		return redirect('/admin/login/')

def addDonasi(request, programId):
	data 			= {'admin_proyek_active' : 'active'}
	error_msg		= []
	if auth(request):
		program 		= get_object_or_404(Program, id=programId)
		donatur         = Donatur.objects.filter(program=program)
		tanggal 		= datetime.date.today()
		data			= {'program': program, 'donatur': donatur, 'tanggal': tanggal, 'admin_proyek_active' : 'active'}

		if request.method == 'POST':
			tanggal 			= request.POST.get('tanggal')
			print(tanggal)
			status_donatur 		= request.POST.get('status_donatur')
			status_donasi 		= request.POST.get('donasi')
			via 				= request.POST.get('via')
			konfirmasi			= False
			donatur_select		= None
			msg 				= None

			if not request.POST.get('konfirmasi') == None:
				konfirmasi = True

			if status_donatur == '1':
				id_donatur 			= request.POST.get('donatur')
				donatur_select 		= get_object_or_404(Donatur, id=id_donatur)
			elif status_donatur == '2':
				nama_donatur 		= validator.escape(request.POST.get('nama_donatur'))
				email_donatur 		= validator.escape(request.POST.get('email'))
				telepon 			= validator.escape(request.POST.get('nohp'))

				vNama_donatur		= not validator.is_empty(nama_donatur)
				vEmail_donatur 		= not validator.is_empty(email_donatur)
				vTelepon 			= not validator.is_empty(telepon)

				valid = vNama_donatur and vEmail_donatur and vTelepon

				if valid :
					donatur_select 		= Donatur(program=program,nama=nama_donatur, email=email_donatur, nohp=telepon)
					donatur_select.save()
				else:
					error_msg.append('Data Donatur tidak valid')

			else:
				error_msg.append('Anda belum memilih "Donatur"')

			if status_donasi == '1':
				bank 				= validator.escape(request.POST.get('nama_bank'))
				nominal 			= validator.escape(request.POST.get('nominal'))
				bukti_transfer 		= request.FILES['bukti_transfer']

				vBank 				= not validator.is_empty(bank)
				vNominal 			= not validator.is_empty(nominal)

				valid = vBank and vNominal

				if valid:
					donasi_donatur 		= Donasi_donatur(donatur=donatur_select, tanggal=tanggal,konfirmasi=konfirmasi, via=via)
					donasi_donatur.save()
					donasi_uang 		= Donasi_uang(donasi_donatur=donasi_donatur,bank=bank, jumlah_uang=nominal, bukti_transfer=bukti_transfer)
					donasi_uang.save()
					msg = 'uang'
					data = { 'sukses': 'Berhasil menambahkan donasi ' + "'" + msg + "'" + ' dari donatur ' + donatur_select.nama + ' pada program ' + program.nama}
					return render(request, 'admin/donasi/newDonasi.html', data)
				else:
					error_msg.append('Data "Donasi Uang" tidak valid')

			elif status_donasi == '2':
				keterangan_buku 	= validator.escape(request.POST.get('keterangan_buku'))

				vKet = not validator.is_empty(keterangan_buku)

				if vKet:
					donasi_donatur 		= Donasi_donatur(donatur=donatur_select, tanggal=tanggal,konfirmasi=konfirmasi, via=via)
					donasi_donatur.save()
					donasi_buku 		= Donasi_buku(donasi_donatur=donasi_donatur,keterangan=keterangan_buku)
					donasi_buku.save()
					msg = 'buku'
					data = { 'sukses': 'Berhasil menambahkan donasi ' + "'" + msg + "'" + ' dari donatur ' + donatur_select.nama + ' pada program ' + program.nama}
					return render(request, 'admin/donasi/newDonasi.html', data)
				else:
					error_msg.append('Data keterangan pada "Donasi Buku" tidak valid')
			else:
				error_msg.append('Anda belum memilih "Bentuk Donasi"')


		data = {'program': program, 'donatur': donatur, 'tanggal': tanggal, 'error': error_msg, 'admin_proyek_active' : 'active'}
		return render(request, 'admin/donasi/newDonasi.html', data)

	return render(request, 'admin/donasi/newDonasi.html', data)


def addDonasiExcel(request, programId):

	data 				= {'admin_proyek_active' : 'active'}
	success_msg			= []
	error_msg			= []
	if auth(request):
		program 		= get_object_or_404(Program, id=programId)

		if request.method == 'POST':
			donasi_uang = request.POST.get('donasi_uang')
			donasi_buku = request.POST.get('donasi_buku')
			namafile = request.FILES['namafile']
			temp = Laporan_temp(filecontent=namafile)
			temp.save()
			path = str(temp.filecontent)

			if donasi_uang == '1':
				try:
					success_msg.append(unggahExcel(path, 1, program))
				except Exception as e:
					print(e)
					error_msg.append(e)

			if donasi_buku == '2':
				try:
					success_msg.append(unggahExcel(path, 2, program))
				except Exception as e:
					print(e)
					error_msg.append(e)
			if not(donasi_uang == '1' or donasi_buku == '2'):
				error_msg.append("Anda belum memilih 'Bentuk Donasi'")
			os.remove(path)
			temp.delete()
			print('post')
		data = {'program': program, 'success_msg': success_msg,'error_msg': error_msg,  'admin_proyek_active' : 'active'}
		return render(request, 'admin/donasi/newDonasiExcel.html', data)

def unggahExcel(path, tipe, program):

	if tipe == 1 :
		b = xlrd.open_workbook(path)
		sheet = b.sheet_by_index(0)
		nrows = sheet.nrows
		ncols = sheet.ncols
		print(nrows)
		print(ncols)
		count = 0
		temp_list_donasi = []

		for r in range(13, nrows):

			tanggal 		 = sheet.cell_value(r,1)
			nama_donatur 	 = sheet.cell_value(r,3)
			email_donatur 	 = sheet.cell_value(r,4)
			telepon 		 = sheet.cell_value(r,5)
			nominal 		 = sheet.cell_value(r,6)
			bank 			 = sheet.cell_value(r,7)
			via 			 = sheet.cell_value(r,8)
			konfirmasi 		 = sheet.cell_value(r,9)


			if validator.is_empty(tanggal):
				raise ValueError('sheet uang, baris ' + str(r+1) +', kolom 2' +' : isi kolom Tanggal tidak boleh kosong.')
			else :
				tanggal 	= datetime.datetime.utcfromtimestamp((tanggal - 25569) * 86400.0).strftime('%Y-%m-%d')


			if validator.is_empty(nama_donatur):
				raise ValueError('sheet uang, baris ' + str(r+1) +', kolom 3' +' : isi kolom Nama Donatur tidak boleh kosong.')

			if validator.is_empty(email_donatur):
				email_donatur = None

			if validator.is_empty(telepon):
				telepon = None

			if validator.is_empty(nominal):
				raise ValueError('sheet uang, baris ' + str(r+1) +', kolom 6' +' : isi kolom Nominal tidak boleh kosong.')

			if validator.is_empty(bank):
				bank = None

			if validator.is_empty(via):
				via = False

			if konfirmasi.lower() == 'ya':
				konfirmasi = True
			elif konfirmasi.lower() == 'tidak' or konfirmasi.lower() == '':
				konfirmasi = False
			else:
				raise ValueError("sheet uang, baris " + str(r+1) +", kolom 9" +" : isi kolom Konfirmasi harus 'ya' atau 'tidak'.")

			item = {'tanggal': tanggal, 'nama_donatur': nama_donatur,
					'email_donatur': email_donatur, 'telepon': telepon,
					'nominal': nominal,'bank': bank, 'via': via, 'konfirmasi': konfirmasi}

			temp_list_donasi.append(item)
			count += 1

		for item in temp_list_donasi:
			donatur_select 		= Donatur(program=program,nama=item['nama_donatur'], email=item['email_donatur'], nohp=item['telepon'])
			donatur_select.save()

			donasi_donatur 		= Donasi_donatur(donatur=donatur_select, tanggal=item['tanggal'],konfirmasi=item['konfirmasi'], via=item['via'])
			donasi_donatur.save()
			donasi_uang 		= Donasi_uang(donasi_donatur=donasi_donatur,bank=item['bank'], jumlah_uang=item['nominal'], bukti_transfer=None)
			donasi_uang.save()

		return str(len(temp_list_donasi)) + ' data donatur uang berhasil disimpan'

	elif tipe == 2 :
		b = xlrd.open_workbook(path)
		sheet = b.sheet_by_index(1)
		nrows = sheet.nrows
		ncols = sheet.ncols
		print(nrows)
		print(ncols)
		counter = 0
		temp_list_donasi = []

		for r in range(13, nrows):

			tanggal 		= sheet.cell_value(r,1)
			nama_donatur 	= sheet.cell_value(r,3)
			email_donatur 	= sheet.cell_value(r,4)
			telepon 		= sheet.cell_value(r,5)
			keterangan 		= sheet.cell_value(r,6)
			via 			= sheet.cell_value(r,7)
			konfirmasi 		= sheet.cell_value(r,8)


			if validator.is_empty(tanggal):
				raise ValueError('sheet buku, baris ' + str(r+1) +', kolom 2' +' : isi kolom Tanggal tidak boleh kosong.')
			else :
				tanggal 	= datetime.datetime.utcfromtimestamp((tanggal - 25569) * 86400.0).strftime('%Y-%m-%d')


			if validator.is_empty(nama_donatur):
				raise ValueError('sheet buku, baris ' + str(r+1) +', kolom 3' +' : isi kolom Nama Donatur tidak boleh kosong.')

			if validator.is_empty(email_donatur):
				email_donatur = None

			if validator.is_empty(telepon):
				telepon = None

			# if validator.is_empty(keterangan):
			# 	raise ValueError('sheet buku, baris ' + str(r+1) +', kolom 6' +' : isi kolom Keterangan tidak boleh kosong.')

			if validator.is_empty(via):
				via = False

			if konfirmasi.lower() == 'ya':
				konfirmasi = True
			elif konfirmasi.lower() == 'tidak':
				konfirmasi = False
			else:
				raise ValueError("sheet buku, baris " + str(r+1) +", kolom 8" +" : isi kolom Konfirmasi harus 'ya' atau 'tidak'.")

			donatur_select 		= Donatur(program=program,nama=nama_donatur, email=email_donatur, nohp=telepon)
			donatur_select.save()

			donasi_donatur 		= Donasi_donatur(donatur=donatur_select, tanggal=tanggal,konfirmasi=konfirmasi, via=via)
			donasi_donatur.save()
			donasi_buku 		= Donasi_buku(donasi_donatur=donasi_donatur,keterangan=keterangan)
			donasi_buku.save()

			item = {'tanggal': tanggal, 'nama_donatur': nama_donatur,
					'email_donatur': email_donatur, 'telepon': telepon,
					'keterangan': keterangan,'via': via, 'konfirmasi': konfirmasi}
			temp_list_donasi.append(item)
			counter += 1

		for item in temp_list_donasi:
			donatur_select 		= Donatur(program=program,nama=item['nama_donatur'], email=item['email_donatur'], nohp=item['telepon'])
			donatur_select.save()

			donasi_donatur 		= Donasi_donatur(donatur=donatur_select, tanggal=item['tanggal'],konfirmasi=item['konfirmasi'], via=item['via'])
			donasi_donatur.save()
			donasi_buku 		= Donasi_buku(donasi_donatur=donasi_donatur,keterangan=item['keterangan'])
			donasi_buku.save()

		return str(len(temp_list_donasi)) + ' data donatur buku berhasil disimpan'

	else:
		raise Exception('jenis donasi tidak ada')

