import os
import xlrd


def index(request):

    programId = request.GET.get('programId')
    program = Program.objects.get(id=programId)
    laporan = Laporan.objects.get(program=program)
    laporan_item = Laporan_item.objects.filter(laporan=laporan)
    jumlah_debit = 0
    jumlah_kredit = 0
    for a in laporan_item:
        if a.isDebit:
            jumlah_debit = jumlah_debit + a.nominal
        if a.isKredit:
            jumlah_kredit = jumlah_kredit + a.nominal
    saldo = jumlah_debit - jumlah_kredit

    data = {
        'program': program,
        'laporan' : laporan_item,
        'saldo': saldo, 
        'jumlah_kredit': jumlah_kredit, 
        'jumlah_debit': jumlah_debit
    }

    if auth(request):
        return render(request, 'admin/laporan/laporan.html', data)
    else :
        return redirect('/admin/login/')

def addKeteranganLaporan(request, programId):
    data = {}
    if auth(request):
        program = Program.objects.get(id=programId)
        laporan = Laporan.objects.get(program=program)

        if request.method == 'GET':
            if laporan.keterangan != None:
                data = {'program': program, 'text': laporan.keterangan}
            else:
                data = {'program': program}
        else:
            text = request.POST.get('text')
            laporan.keterangan = text
            laporan.save()
            data = {'program': program, 'text': laporan.keterangan, 'alert_msg':True, 'alert_success': True}
    return render(request,'admin/laporan/add-keterangan.html', data)

def addLaporan(request):
    alert = False
    alert_msg = ''
    data = {}
    if auth(request):
        try:
            programId = request.GET.get('programId')
            program = Program.objects.get(id=programId)
            laporan = Laporan.objects.get(program=program)
            laporan_item = Laporan_item.objects.filter(laporan=laporan)
            
        except Exception as e:
            print(e)
            laporan = None
            laporan_item = None

        if request.method == 'POST':
            try:
                alert_msg = "Laporan berhasil di-upload"
                namafile = request.FILES['namafile']
                temp = Laporan_temp(filecontent=namafile)
                temp.save()
                b = xlrd.open_workbook(str(temp.filecontent))
                os.remove(str(temp.filecontent))
                temp.delete()

                sheet = b.sheet_by_index(0)
                nrows = sheet.nrows
                ncols = sheet.ncols

                if laporan_item:
                    alert_msg = "Laporan berhasil diganti"
                    for item in laporan_item:
                        item.delete()

                if not laporan:
                    laporan = Laporan(program=program)
                    laporan.save()

                for r in range(4, nrows):

                        keterangan = sheet.cell_value(r,0)
                        print(sheet.cell_value(r,0))
                        type(sheet.cell_value(r,0))
                        print(sheet.cell_value(r,1))
                        type(sheet.cell_value(r,1))
                        print(sheet.cell_value(r,2))
                        type(sheet.cell_value(r,1))

                        if(sheet.cell_value(r,1)!=''):
                            isDebit = True
                            nominal = int(sheet.cell_value(r,1))
                        else:
                            isDebit = False

                        if(sheet.cell_value(r,2)!=''):
                            isKredit = True
                            nominal = int(sheet.cell_value(r,2))
                        else:
                            isKredit = False

                        
                        item = Laporan_item(laporan=laporan, keterangan=keterangan, nominal=nominal, isDebit=isDebit, isKredit=isKredit)
                        item.save()
                alert = True
            except Exception as e:
                print(e)
                alert_msg = "Laporan gagal di-upload. Pastikan yang di-upload adalah file Excel"
                alert = True
            
            print(alert)
            print(alert_msg)

        data = {
            'alert_success': alert,
            'alert_msg' : alert_msg,
            'program': program,
            'laporan': laporan
            }
            
        return render(request, 'admin/laporan/newLaporan.html', data)  
    else :
        return render(request, 'admin/laporan/newLaporan.html') 

def editLaporan(request):

    data = {}

    if auth(request):
        if request.method == 'GET':
            programId = request.GET.get('programId')
            try:
                Program.objects.filter(id=programId).delete()
                return render(request, 'admin/program/viewProgram.html', data)
            except:
                return redirect('/admin/program/')      
    else :
        return redirect('/admin/login/')

def deleteLaporan(request):

    data = {}

    if auth(request):
        if request.method == 'GET':
            programId = request.GET.get('programId')
            try:
                Program.objects.filter(id=programId).delete()
                return render(request, 'admin/program/viewProgram.html', data)
            except:
                return redirect('/admin/program/')      
    else :
        return redirect('/admin/login/')