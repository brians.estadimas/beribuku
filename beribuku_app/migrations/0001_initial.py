# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='About',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('judul', models.CharField(max_length=100)),
                ('deskripsi', models.CharField(max_length=1200)),
            ],
        ),
        migrations.CreateModel(
            name='Akun',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('username', models.CharField(max_length=100)),
                ('password', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Barang',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('nama', models.CharField(max_length=200)),
                ('jumlah', models.CharField(null=True, max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Donasi',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('deadline', models.DateField()),
            ],
        ),
        migrations.CreateModel(
            name='Donasi_buku',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('keterangan', models.CharField(null=True, max_length=500)),
            ],
        ),
        migrations.CreateModel(
            name='Donasi_donatur',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('tanggal', models.DateField()),
                ('konfirmasi', models.BooleanField()),
                ('via', models.CharField(null=True, max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Donasi_uang',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('bank', models.CharField(null=True, max_length=50)),
                ('jumlah_uang', models.IntegerField()),
                ('bukti_transfer', models.FileField(upload_to='static/foto/bukti_transfer')),
                ('donasi_donatur', models.ForeignKey(to='beribuku_app.Donasi_donatur')),
            ],
        ),
        migrations.CreateModel(
            name='Donatur',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('nama', models.CharField(max_length=50)),
                ('email', models.CharField(null=True, max_length=50)),
                ('nohp', models.CharField(null=True, max_length=15)),
            ],
        ),
        migrations.CreateModel(
            name='FileMailTemp',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('fileUpload', models.FileField(upload_to='static/files')),
            ],
        ),
        migrations.CreateModel(
            name='FotoProgram',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('gambar', models.FileField(upload_to='static/images/proyek')),
                ('caption', models.CharField(max_length=250, default='')),
            ],
        ),
        migrations.CreateModel(
            name='Laporan',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('keterangan', models.TextField(null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Laporan_item',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('keterangan', models.CharField(max_length=1000)),
                ('nominal', models.IntegerField()),
                ('isDebit', models.BooleanField()),
                ('isKredit', models.BooleanField()),
                ('laporan', models.ForeignKey(to='beribuku_app.Laporan')),
            ],
        ),
        migrations.CreateModel(
            name='Laporan_temp',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('filecontent', models.FileField(upload_to='static/files')),
            ],
        ),
        migrations.CreateModel(
            name='Nama',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('namaDepan', models.CharField(max_length=100, validators=[django.core.validators.RegexValidator('^[a-zA-Z]*$', 'Only alphanumeric characters are allowed.')])),
                ('namaBelakang', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Program',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('nama', models.CharField(max_length=200)),
                ('deskripsi', models.TextField()),
                ('target', models.CharField(max_length=200)),
                ('partner', models.CharField(max_length=200)),
                ('logoPartner', models.FileField(upload_to='static/images/logo')),
            ],
        ),
        migrations.CreateModel(
            name='Rekening',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('nama', models.CharField(max_length=50)),
                ('noRekening', models.CharField(max_length=50)),
                ('bank', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Subscriber',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('date', models.DateTimeField()),
                ('phone', models.IntegerField()),
                ('email', models.EmailField(max_length=254)),
            ],
        ),
        migrations.CreateModel(
            name='Test',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('a', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Tim',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('nama', models.CharField(max_length=50)),
                ('email', models.CharField(max_length=50)),
                ('jabatan', models.CharField(max_length=200)),
                ('foto', models.FileField(upload_to='static/images/tim')),
            ],
        ),
        migrations.AddField(
            model_name='laporan',
            name='program',
            field=models.ForeignKey(to='beribuku_app.Program'),
        ),
        migrations.AddField(
            model_name='fotoprogram',
            name='program',
            field=models.ForeignKey(to='beribuku_app.Program'),
        ),
        migrations.AddField(
            model_name='donatur',
            name='program',
            field=models.ForeignKey(to='beribuku_app.Program'),
        ),
        migrations.AddField(
            model_name='donasi_donatur',
            name='donatur',
            field=models.ForeignKey(to='beribuku_app.Donatur'),
        ),
        migrations.AddField(
            model_name='donasi_buku',
            name='donasi_donatur',
            field=models.ForeignKey(to='beribuku_app.Donasi_donatur'),
        ),
        migrations.AddField(
            model_name='donasi',
            name='program',
            field=models.ForeignKey(to='beribuku_app.Program'),
        ),
        migrations.AddField(
            model_name='donasi',
            name='rekening',
            field=models.ForeignKey(to='beribuku_app.Rekening'),
        ),
        migrations.AddField(
            model_name='barang',
            name='program',
            field=models.ForeignKey(to='beribuku_app.Program'),
        ),
    ]
